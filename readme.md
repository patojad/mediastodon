# Mediastodon

Originalmente Kim escribió esta sencilla aplicación que usa la API de Mastodon para obtener gatitos.

> A partir de ese código creado por Kim, he hecho una pequeña modificación para mostrar capturas de pantalla de GNU/Linux mostrando su mejor composición de fondo de escritorio, iconos, temas, etc...
Recopila las capturas de pantalla que se comparten en Mastodon con la etiqueta #unixporn y las muestra utilizando el servicio de GitLab Pages
>
> Puedes ver en acción la web en este enlace:
> * https://victorhck.gitlab.io/unixporn/

En Base al trabajo de victorhck y Kim (Como fue compartelibre) intente generar un buscador de imagenes por hashtag en Mastodon

## Licencia

Esta movida está escrita bajo la licencia gnu gpl v3 se libre de copiar, modificar y distribuir. Free as in freedom ;)
